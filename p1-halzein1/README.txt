Hussein Alzein - halzein1@binghamton.edu
Code was tested on bingsuns using port 1842 (hardcoded port, didn't know if we needed to pass port in as a parameter but this works).
EXECUTION:
part 1: 
make
./telnetserv
./telnetcli <YOUR IP ADDRESS HERE>

part2:
make
./mono <all arguments>

Special notes:
One issue exists that I couldn't fix where if you try to do ls in an empty directory, no more reading and writing can occur and both server and client become unresponsive.
